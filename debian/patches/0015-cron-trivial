Index: refpolicy-2.20220325/policy/modules/services/cron.if
===================================================================
--- refpolicy-2.20220325.orig/policy/modules/services/cron.if
+++ refpolicy-2.20220325/policy/modules/services/cron.if
@@ -775,6 +775,24 @@ interface(`cron_rw_tmp_files',`
 
 ########################################
 ## <summary>
+##      Read and write inherited crond temporary files.
+## </summary>
+## <param name="domain">
+##      <summary>
+##      Domain allowed access.
+##      </summary>
+## </param>
+#
+interface(`cron_rw_inherited_tmp_files',`
+	gen_require(`
+		type crond_tmp_t;
+	')
+
+	allow $1 crond_tmp_t:file rw_inherited_file_perms;
+')
+
+########################################
+## <summary>
 ##	Read system cron job lib files.
 ## </summary>
 ## <param name="domain">
@@ -907,6 +925,24 @@ interface(`cron_dontaudit_append_system_
 ')
 
 ########################################
+## <summary>
+##	allow appending temporary system cron job files.
+## </summary>
+## <param name="domain">
+##	<summary>
+##	Domain to allow.
+##	</summary>
+## </param>
+#
+interface(`cron_append_system_job_tmp_files',`
+	gen_require(`
+		type system_cronjob_tmp_t;
+	')
+
+	allow $1 system_cronjob_tmp_t:file append_file_perms;
+')
+
+########################################
 ## <summary>
 ##	Read and write to inherited system cron job temporary files.
 ## </summary>
Index: refpolicy-2.20220325/policy/modules/services/cron.te
===================================================================
--- refpolicy-2.20220325.orig/policy/modules/services/cron.te
+++ refpolicy-2.20220325/policy/modules/services/cron.te
@@ -431,6 +431,8 @@ optional_policy(`
 	systemd_dbus_chat_logind(system_cronjob_t)
 	systemd_read_journal_files(system_cronjob_t)
 	systemd_write_inherited_logind_sessions_pipes(system_cronjob_t)
+	# for runuser
+	init_search_keys(system_cronjob_t)
 	# so cron jobs can restart daemons
 	init_stream_connect(system_cronjob_t)
 	init_manage_script_service(system_cronjob_t)
@@ -501,6 +503,7 @@ corenet_udp_sendrecv_generic_if(system_c
 corenet_tcp_sendrecv_generic_node(system_cronjob_t)
 corenet_udp_sendrecv_generic_node(system_cronjob_t)
 corenet_udp_bind_generic_node(system_cronjob_t)
+corenet_tcp_connect_tor_port(system_cronjob_t)
 
 dev_getattr_all_blk_files(system_cronjob_t)
 dev_getattr_all_chr_files(system_cronjob_t)
@@ -551,6 +554,7 @@ logging_manage_generic_logs(system_cronj
 logging_send_audit_msgs(system_cronjob_t)
 logging_send_syslog_msg(system_cronjob_t)
 
+miscfiles_read_generic_certs(system_cronjob_t)
 miscfiles_read_localization(system_cronjob_t)
 
 seutil_read_config(system_cronjob_t)
@@ -671,10 +675,16 @@ optional_policy(`
 ')
 
 optional_policy(`
+	ntp_read_config(system_cronjob_t)
+')
+
+optional_policy(`
 	userdom_user_home_dir_filetrans_user_home_content(system_cronjob_t, { dir file lnk_file fifo_file sock_file })
 
 	# for gpg-connect-agent to access /run/user/0
 	userdom_manage_user_runtime_dirs(system_cronjob_t)
+	# for /run/user/0/gnupg
+	userdom_manage_user_tmp_dirs(system_cronjob_t)
 ')
 
 ########################################
Index: refpolicy-2.20220325/policy/modules/system/init.if
===================================================================
--- refpolicy-2.20220325.orig/policy/modules/system/init.if
+++ refpolicy-2.20220325/policy/modules/system/init.if
@@ -3772,3 +3772,21 @@ interface(`init_getrlimit',`
 
 	allow $1 init_t:process getrlimit;
 ')
+
+########################################
+## <summary>
+##      Allow searching init_t keys
+## </summary>
+## <param name="domain">
+##      <summary>
+##      Source domain
+##      </summary>
+## </param>
+#
+interface(`init_search_keys',`
+	gen_require(`
+		type init_t;
+	')
+
+	allow $1 init_t:key search;
+')
Index: refpolicy-2.20220325/policy/modules/services/mta.te
===================================================================
--- refpolicy-2.20220325.orig/policy/modules/services/mta.te
+++ refpolicy-2.20220325/policy/modules/services/mta.te
@@ -284,7 +284,12 @@ optional_policy(`
 	userdom_dontaudit_use_user_ptys(system_mail_t)
 
 	optional_policy(`
+ifdef(`hide_broken_symptoms',`
+		# anacron on Debian gives empty email if this is not permitted
+		cron_append_system_job_tmp_files(system_mail_t)
+', `
 		cron_dontaudit_append_system_job_tmp_files(system_mail_t)
+')
 	')
 ')
 
Index: refpolicy-2.20220325/policy/modules/services/postfix.te
===================================================================
--- refpolicy-2.20220325.orig/policy/modules/services/postfix.te
+++ refpolicy-2.20220325/policy/modules/services/postfix.te
@@ -642,6 +642,7 @@ optional_policy(`
 
 optional_policy(`
 	cron_system_entry(postfix_postdrop_t, postfix_postdrop_exec_t)
+	cron_use_system_job_fds(postfix_postdrop_t)
 ')
 
 optional_policy(`
